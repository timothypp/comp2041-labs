#!/usr/bin/perl -w

$word_to_search = "";

if (@ARGV == 1){
	$word_to_search = $ARGV[0];
	$word_to_search = lc($word_to_search);
}
else{
	die "Usage: $0 <word>\n";
}

foreach $file (glob "poets/*.txt") {
	@words = ();
	$number_of_occurrence = 0;
	$total_words = 0;

    if (open(F,"<$file")){
    	@line_data = <F>;

    	foreach $line (@line_data){
			chomp($line);
			my @words_temp = split /[^A-Za-z]/, $line;

			foreach $splitted (@words_temp){
				if ($splitted =~  /^\n*$/g || $splitted =~  /^ *$/g){ }
				else{
					$splitted = lc($splitted);
					push(@words, $splitted);
				}
			}
		}

		foreach $line (@words){
			$total_words = $total_words + 1;
			$line = lc($line);
			
			if ($line eq $word_to_search){
				$number_of_occurrence = $number_of_occurrence + 1;
			}
		}
    }

    $author = $file;
    $author =~ s/_/ /g;
    $author =~ s/\.txt//g;
    $author =~ s/poets\///g;
    $percentage = int($number_of_occurrence) / int ($total_words);

    printf "%4d/%6d = %.9f %s\n", $number_of_occurrence, $total_words, $percentage, $author; 
}
