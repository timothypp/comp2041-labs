#!/usr/bin/python

import sys, re

words = []
word_to_search = ""
number_of_occurrence = 0

if (len(sys.argv) == 2):
	word_to_search = sys.argv[1]
	word_to_search = word_to_search.lower()
else:
	print >>sys.stderr, "Usage: %s <word>" % sys.argv[0]
	sys.exit(1)


for line in sys.stdin:
	line.rstrip()
	words_temp = re.split(r'[^A-Za-z]', line)

	for splitted in words_temp:
		matcher_1 = re.match(r'^\n*$', splitted)
		matcher_2 = re.match(r'^ *$', splitted)

		if not (matcher_1 or matcher_2):
			splitted = splitted.lower()
			words.append(splitted)

for line in words:
	line = line.lower()
	regex = r"^" + word_to_search + r"$"

	if re.search(regex, line, re.IGNORECASE):
		number_of_occurrence = number_of_occurrence + 1

if (number_of_occurrence > 1):
	print word_to_search, "occurred", number_of_occurrence, "times"
else:
	print word_to_search, "occurred", number_of_occurrence, "time"
