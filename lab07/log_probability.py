#!/usr/bin/python

import re, sys, math, glob

word_to_search = ""

if (len(sys.argv) == 2):
	word_to_search = sys.argv[1];
	word_to_search = word_to_search.lower()
else:
	print >>sys.stderr, "Usage: %s <word>" % sys.argv[0]
	sys.exit(1)

for filename in glob.glob("poets/*.txt"):
	words = []
	number_of_occurrence = 0
	total_words = 0

	try:
		inf = open(filename)
	except:
		print >>sys.stderr, "%s: can't open", filename,"\n" % sys.argv[0]
	else:
		try:
			for line in open(filename):
				line.rstrip()
				words_temp = re.split(r'[^A-Za-z]', line)

				for splitted in words_temp:
					matcher_1 = re.match(r'^\n*$', splitted)
					matcher_2 = re.match(r'^ *$', splitted)

					if not (matcher_1 or matcher_2):
						splitted = splitted.lower()
						words.append(splitted)

			for line in words:
				total_words = total_words + 1
				line = line.lower()

				regex = r"^" + word_to_search + r"$"

				if re.search(regex, line, re.IGNORECASE):
					number_of_occurrence = number_of_occurrence + 1
		finally:
				inf.close()

	author = filename
	author = re.sub(r'_', 		' ', 	author, flags=re.I)
	author = re.sub(r'\.txt', 	'', 	author, flags=re.I)
	author = re.sub(r'poets\/', '', 	author, flags=re.I)

	percentage = math.log(float(number_of_occurrence + 1) / float(total_words))

	print ("log((%d+1)/%6d) = %8.4f %s"  % (number_of_occurrence, total_words, percentage, author))
