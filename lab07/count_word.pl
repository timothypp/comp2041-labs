#!/usr/bin/perl -w

@words = ();
$word_to_search = "";
$number_of_occurrence = 0;

if (@ARGV == 1){
	$word_to_search = $ARGV[0];
	$word_to_search = lc($word_to_search);
}
else{
	die "Usage: $0 <word>\n";
}

while ($line = <STDIN>){
	chomp($line);
	my @words_temp = split /[^A-Za-z]/, $line;

	foreach $splitted (@words_temp){
		if ($splitted =~  /^\n*$/g || $splitted =~  /^ *$/g){ }
		else{
			$splitted = lc($splitted);
			push(@words, $splitted);
		}
	}
}

foreach $line (@words){
	$line = lc($line);
	
	if ($line eq $word_to_search){
		$number_of_occurrence = $number_of_occurrence + 1;
	}
}

if ($number_of_occurrence > 1){
	print "$word_to_search occurred $number_of_occurrence times\n";
}
else{
	print "$word_to_search occurred $number_of_occurrence time\n";
}