#!/usr/bin/python

import sys, re, glob, math

filename = []
debug = "no"

if (len(sys.argv) == 2):
	match_arg = re.match(r'^-d$', sys.argv[1])

	if match_arg:
		print >>sys.stderr, "Usage: %s [-d] <poem file>" % sys.argv[0]
		sys.exit(1)
	else:
		filename.append(sys.argv[1])
elif (len(sys.argv) > 2):
	match_arg = re.match(r'^-d$', sys.argv[1])

	if match_arg:
		debug = "yes"

	for file in sys.argv:
		match_arg_2 = re.match(r'^-d$', file)
		match_arg_3 = re.match(sys.argv[0], file)

		if not (match_arg_2 or match_arg_3):
			filename.append(file)
else:
	print >>sys.stderr, "Usage: %s [-d] <poem file>" % sys.argv[0]
	sys.exit(1)
	

for file_main in filename:
	temp_words = []
	all_author = {}

	try:
		inf = open(file_main)
	except:
		print >>sys.stderr, "%s: can't open", file_main,"\n" % sys.argv[0]
	else:
		try:
			myre = re.compile(r"[A-Za-z]+")

			for line in open(file_main):
				line.rstrip()
				line = line.lower()
				temp_1 = myre.findall(line)
				temp_words.extend(temp_1)

			for filename in glob.glob("poets/*.txt"):
				temp_words_poets = []
				all_words_poet = {}
				total_words = 0

				for line in open(filename):
					line.rstrip()
					line = line.lower()
					temp_1 = myre.findall(line)
					temp_words_poets.extend(temp_1)

				total_words = len(temp_words_poets)

				for word_2 in temp_words_poets:
					matcher_1 = re.match(r'^\n*$', word_2)
					matcher_2 = re.match(r'^ *$', word_2)

					#if not (matcher_1 or matcher_2):
					if word_2 in all_words_poet:
						count = int(all_words_poet[word_2]) + 1
						all_words_poet[word_2] = count
					else:
						all_words_poet[word_2] = 1

				calculation = 0
				temp_words.sort()
				temp_words_poets.sort()

				for word_3 in temp_words:
					if word_3 in all_words_poet:
						number_of_occurrence = all_words_poet[word_3]
						percentage = math.log(float(number_of_occurrence + 1) / float(total_words))
						calculation = calculation + percentage
					else:
						no_occurrence = 0
						percentage = math.log(float(no_occurrence + 1) / float(total_words))
						calculation = calculation + percentage

				author = filename
				author = re.sub(r'_', 		' ', 	author, flags=re.I)
				author = re.sub(r'\.txt', 	'', 	author, flags=re.I)
				author = re.sub(r'poets\/', '', 	author, flags=re.I)

				all_author[author] = calculation
		finally:
			inf.close()

			once = False

			for author in sorted(all_author, key=all_author.get, reverse=True):
				value = all_author[author]

				if (debug == "yes"):
					print ("%s: log_probability of %.1f for %s" % (file_main, value, author))
				else:
					if not once:
						print ("%s most resembles the work of %s (log-probability=%.1f)" % (file_main, author, value))
						once = True

			once = False
