#!/usr/bin/perl -w

@filename = ();
$debug = "no";

if (@ARGV == 1){
	die "Usage: $0 [-d] <poem file>\n" if ($ARGV[0] =~ '^-d$');
	push(@filename, $ARGV[0]);
}
elsif (@ARGV > 1){
	$debug = "yes" if ($ARGV[0] =~ '^-d$');

	foreach $file (@ARGV){
		if (not $file =~ '^-d$'){ 
			push(@filename, $file); 
		}
	}
}
else{
	die "Usage: $0 [-d] <poem file>\n";
}

foreach $file_main (@filename){
	#print "$file_main\n";

	@temp_words = ();
	%all_author = ();

	if (open(F,"<$file_main")){
		foreach $line (<F>){
			chomp($line);
			$line = lc($line);
			push @temp_words, $line =~ /[A-Za-z]+/gi;
		}

		foreach $filename (glob "poets/*.txt") {
			#print "$filename\n";
			@temp_words_poets = ();
			%all_words_poet = ();
			$total_words = 0;

			if (open(FS,"<$filename")){
				foreach $line (<FS>){
					chomp($line);
					$line = lc($line);
					push @temp_words_poets, $line =~ /[A-Za-z]+/gi;
				}

				$total_words = @temp_words_poets;
				#print "Total Words : $total_words\n";

				#build hash
				foreach $word_2 (@temp_words_poets){
					if (exists $all_words_poet{$word_2}){
						$count = int($all_words_poet{$word_2}) + 1;
						$all_words_poet{$word_2} = $count;
					}
					else{
						$all_words_poet{$word_2} = 1;
					}
				}

				$calculation = 0;

				foreach $word_3 (sort @temp_words){
					if (exists $all_words_poet{$word_3}){
						$number_of_occurrence = $all_words_poet{$word_3};
					    $percentage = log(int(int($number_of_occurrence) +1) / int (@temp_words_poets));
					    $calculation = $calculation	+ $percentage;
					}
					else{
						$no_occurrence = 0;
					    $percentage = log(int($no_occurrence + 1) / int (@temp_words_poets));
					    $calculation = $calculation	+ $percentage;
					}
				}

			    $author = $filename;
			    $author =~ s/_/ /g;
			    $author =~ s/\.txt//g;
			    $author =~ s/poets\///g;

			    $all_author{$author} = $calculation;

				close FS;
			}
			else{
				die "Cannot open the file : $filename\n";
			}
		}

		my @data_author = sort { $all_author{$b} <=> $all_author{$a} } keys (%all_author);
		if ($debug =~ "yes"){
			foreach my $author (@data_author) {
				$value = $all_author{$author};
				printf "$file_main: log_probability of %.1f for $author\n", $value;
			}
		}
		elsif ($debug =~ "no"){
			$author = $data_author[0];
			$value = $all_author{$data_author[0]};
			printf "$file_main most resembles the work of $author (log-probability=%.1f)\n", $value;
		}

		close F;
	}
	else{
		die "Cannot open the file : $file_main\n";
	}
}

