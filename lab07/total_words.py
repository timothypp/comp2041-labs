#!/usr/bin/python

import sys, re

words = [];

for line in sys.stdin:
	line.rstrip()
	words_temp = re.split(r'[^A-Za-z]', line)

	for splitted in words_temp:
		matcher_1 = re.match(r'^\n*$', splitted)
		matcher_2 = re.match(r'^ *$', splitted)

		if not(matcher_1 or matcher_2):
			words.append(splitted)

number_of_words = len(words)

print number_of_words, " words"
