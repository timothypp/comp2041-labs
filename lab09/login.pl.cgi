#!/usr/bin/perl -w
# Work by : Timothy Putra Pringgondhani (z5043683)

my $is_cgi = defined $ENV{'GATEWAY_INTERFACE'};

if (!$is_cgi){
	my $username = "";
	my $password = "";

	while (!"$username"){
		print "username: ";
		$username = <>;
		chomp($username);
	}

	while (!"$password"){
		print "password: ";
		$password = <>;
		chomp($password);
	}

	check_username_password($username, $password);
}
else{
	use CGI qw/:all/;
	use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

	print header, start_html('Login');
	warningsToBrowser(1);

	my $username = param('username') || '';
	my $password = param('password') || '';

	chomp($username);
	chomp($password);

	if ("$username" && "$username" !~ /[A-Za-z0-9]/){
		print "Invalid username format. Username must be alphanumeric!", "\n";
		print end_html;
		exit(0);
	}

	if ("$username" && "$password"){
		check_username_password($username, $password);
	}
	elsif ("$username" && !"$password"){
	    print start_form, "\n";
	    print hidden(-name => "username", -default => "$username", -override => 1);
	    print "Password:\n", textfield('password'), "\n";
	    print submit(value => Login), "\n";
	    print end_form, "\n";
	}
	elsif (!"$username" && "$password"){
	    print start_form, "\n";
	    print "Username:\n", textfield('username'), "\n";
	    print hidden(-name => "password", -default => "$password", -override => 1);
	    print submit(value => Login), "\n";
	    print end_form, "\n";
	}
	else {
	    print start_form, "\n";
	    print "Username:\n", textfield('username'), "\n";
	    print "Password:\n", textfield('password'), "\n";
	    print submit(value => Login), "\n";
	    print end_form, "\n";
	}

	print end_html;
	exit(0);
}

sub check_username_password {
	my $username = $_[0];
	my $password = $_[1];

	chomp($username);
	chomp($password);

	if (-r "../accounts/$username/password") {
		open(F, "<../accounts/$username/password");
		my $correct_password = <F>;
		chomp($correct_password);

		if ("$password" eq "$correct_password"){
			print "$username authenticated.\n";
		}
		else{
			print "Incorrect password!\n";
		}
	}
	else{
		print "Unknown username!\n"
	}
}
