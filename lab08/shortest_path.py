#!/usr/bin/python2.7

import re, sys

start_location = ""
end_location = ""
graph_list = {}
path_list = {}
distance_list = {}

#DFS (Depth-First Search) is used to find the shortest path
def findPath (graph, start, end, totalDistance, visited):
	for city in graph[start]:

		if not city in visited:
			visited.append(city)
			totalDistance += graph[start][city]

			if city == end:
				key = ''.join(visited)
				path = ' '.join(visited)
				path_list[key] = path
				distance_list[key] = totalDistance
			else:
				findPath(graph, city, end, totalDistance, visited)

			visited.remove(city)
			totalDistance -= graph[start][city]


if (len(sys.argv) < 3):
	print >>sys.stderr, "Usage: %s <Start Location> <End Location>" % sys.argv[0]
	sys.exit(1)
elif (len(sys.argv) >= 3):
	start_location = sys.argv[1]
	end_location = sys.argv[2]

for line in sys.stdin:
	line = line.rstrip('\n')	
	words = line.split()

	nodeA = words[0]
	nodeB = words[1]
	distance = int(words[2])

	nodeA = nodeA.rstrip()
	nodeB = nodeB.rstrip()

	graph_list.setdefault(nodeA, {})
	graph_list.setdefault(nodeB, {})

	graph_list[nodeA][nodeB] = distance
	graph_list[nodeB][nodeA] = distance


#start finding path
totalDistance = 0
visited = [start_location]

findPath(graph_list, start_location, end_location, totalDistance, visited)

#get the key of the lowet distance
temp_list = sorted(distance_list, key=distance_list.get, reverse=False)
key = temp_list[0]
print "Shortest route is length = %s: %s." % (distance_list[key], path_list[key])

