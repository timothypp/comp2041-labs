#!/usr/bin/perl -w

@coursecode = ();
%hash_course = ();
$option = "";
$has_semester_one = "no";
$has_semester_two = "no";
$has_semester_summer = "no";

if (@ARGV == 1){
	die "Usage: $0 [-d | -t] <course code>\n" if ($ARGV[0] =~ '^-d$' || $ARGV[0] =~ '^-t$');
	push(@coursecode, $ARGV[0]);
}
elsif (@ARGV > 1){
	$option = $ARGV[0] if ($ARGV[0] =~ '^-d$' || $ARGV[0] =~ '^-t$');

	foreach $file (@ARGV){
		if (not $file =~ '^-d$' || $file =~ '^-t$'){ 
			push(@coursecode, $file); 
		}
	}
}
else{
	die "Usage: $0 [-d | -t] <course code>\n";
}

foreach $course (@coursecode){
	if ($course =~ '^[A-Za-z]{3,}[0-9]{3,}$'){
		getInformation($course);
	}
	else{
		print "Invalid Course Code : $course\n";
	}
}

if ($option =~ '^-t$'){
	printOption_T();
}






sub getInformation{
	my $the_course = $_[0];
	$the_course = uc($the_course);

	$url = "http://www.timetable.unsw.edu.au/current/" . $the_course . ".html";
	#open F, "wget -q -O- $url|" or die;
	open $fh, "-|", "wget -q -O- $url" or die;	#alternative to above

	my $found_list = 1;				# 0 : true | 1 : false
	my $found_lecture = 1;			# 0 : true | 1 : false
	my $counter = 0;				# start counting
	my $semester = "";
	my @schedule_list = ();
	my %semester_list = ();

	#while ($line = <F>) {
	while ($line = <$fh>) {						#alternative to above
		chomp ($line);

		if ($line =~ '<tr class="rowLowlight">' || $line =~ '<tr class="rowHighlight">'){
			$found_list = 0;		#true
			$found_lecture = 1;		#reset
			$counter = 0;			#reset
			$semester = "";			#reset
			next;
		}

		if ($found_list == 0){
			if ($line =~ 'lecture' || $line =~ 'Lecture'){
				$found_lecture = 0;		#true
				$counter = 0;			#reset
				$semester = "";			#reset
				next;
			}
			elsif ($found_lecture == 0){
				$counter = $counter + 1;
				
				#print "$line\n";

				if ($counter == 1){
					$semester = $line;
					$semester =~ s/<td.*">//g;
					$semester =~ s/<.*>//g;
					$semester =~ s/\s+//g;
					$semester =~ s/T/S/g;
					$semester =~ s/U/X/g;
				}
				elsif ($counter == 6){
					$schedule = $line;
					$schedule =~ s/<td.*">//g;
					$schedule =~ s/<.*>//g;
					$schedule =~ s/^\s+//g;
					$schedule =~ s/\s+$//g;

					if (not $schedule eq '') {
						if (not exists $semester_list{"$schedule"}){
							$semester_list{$schedule} = $semester;

							push (@schedule_list, $schedule);
						}
					}
				}
			}
			else{ 
				$found_list = 1; 		#reset
				$found_lecture = 1;		#reset
				$counter = 0;			#reset
				$semester = "";			#reset
			}
		}
	}

	if ($option =~ '^-d$'){
		printOption_D($the_course, \@schedule_list, \%semester_list);
	}
	elsif($option =~ '^-t$'){ 
		prepareOption_T($the_course, \@schedule_list, \%semester_list);
	}
	else{
		printStandard($the_course, \@schedule_list, \%semester_list);
	}

	close $fh;
}





sub printStandard{
	my ($the_course, $schedule_list, $semester_list) = @_;
	my %semester_listy = %$semester_list;

	foreach $schedule (@$schedule_list) {
		$semester_data = $semester_listy{$schedule};
		print "$the_course: $semester_data $schedule\n";
	}
}





sub printOption_D{
	my ($the_course, $schedule_list, $semester_list) = @_;
	my %semester_listy = %$semester_list;

	foreach $schedule (@$schedule_list) {
		my $semester_data = $semester_listy{$schedule};

		@splitter = split(',', $schedule);

		foreach $split (@splitter){
			if ($split =~ ':'){

				$split =~ s/\(.*//gi;
				$split =~ s/^\s+//gi;

				@splitter_time = split(' ', $split);

				$day = $splitter_time[0];
				$start_time = "";
				$end_time = "";
				$value_start = 0;
				$value_end = 0;

				for ($i = 0; $i < @splitter_time; $i++){
					if ($i == 1){
						$start_time = $splitter_time[$i];
						$start_time =~ s/:/./;
						$value_start = int ($start_time);
					}
					elsif ($i == 3){
						$end_time = $splitter_time[$i];
						$end_time =~ s/:/./;
						$value_end = int ($end_time);
					}
				}

				if ($end_time =~ '.00$'){
					for ($i = $value_start; $i < $value_end; $i++){
						print "$semester_data $the_course $day $i\n";
					}
				}
				else{
					for ($i = $value_start; $i <= $value_end; $i++){
						print "$semester_data $the_course $day $i\n";
					}
				}
			}
		}
	}	
}





sub prepareOption_T{
my ($the_course, $schedule_list, $semester_list) = @_;
	my %semester_listy = %$semester_list;

	foreach $schedule (@$schedule_list) {
		my $semester_data = $semester_listy{$schedule};

		@splitter = split(',', $schedule);

		foreach $split (@splitter){
			if ($split =~ ':'){

				$split =~ s/\(.*//gi;
				$split =~ s/^\s+//gi;

				@splitter_time = split(' ', $split);

				$day = $splitter_time[0];
				$start_time = "";
				$end_time = "";
				$value_start = 0;
				$value_end = 0;

				for ($i = 0; $i < @splitter_time; $i++){
					if ($i == 1){
						$start_time = $splitter_time[$i];
						$start_time =~ s/:/./;
						$value_start = int ($start_time);
					}
					elsif ($i == 3){
						$end_time = $splitter_time[$i];
						$end_time =~ s/:/./;
						$value_end = int ($end_time);
					}
				}

				if ($end_time =~ '.00$'){
					for ($i = $value_start; $i < $value_end; $i++){
						$make = "$semester_data" . "_" . "$day" . "_" . "$i";

						if (exists $hash_course{$make}){
							$sum = $hash_course{$make};
							$sum = $sum + 1;
							$hash_course{$make} = $sum;
						}
						else{
							$hash_course{$make} = 1;
						}
					}
				}
				else{
					for ($i = $value_start; $i <= $value_end; $i++){
						$make = "$semester_data" . "_" . "$day" . "_" . "$i";

						if (exists $hash_course{$make}){
							$sum = $hash_course{$make};
							$sum = $sum + 1;
							$hash_course{$make} = $sum;
						}
						else{
							$hash_course{$make} = 1;
						}
					}
				}

				if ($semester_data eq "S1"){ $has_semester_one = "yes"; }
				if ($semester_data eq "S2"){ $has_semester_two = "yes"; }
				if ($semester_data eq "X1"){ $has_semester_summer = "yes"; }
			}
		}
	}	
}





sub printOption_T{
	#semester 1
	if ($has_semester_one eq "yes"){		
		printHelper("S1");
	}

	#semester 2
	if ($has_semester_two eq "yes"){
		printHelper("S2");
	}

	#semester summer
	if ($has_semester_summer eq "yes"){
		printHelper("X1");
	}
}

#print helper
sub printHelper{
	my @day_list = ("Mon", "Tue", "Wed", "Thu", "Fri");
	my @time_list = ("09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00");
	my $semester_info = $_[0];

	print "$semester_info" . "\t\tMon\tTue\tWed\tThu\tFri\n";
		
	foreach $time (@time_list){
		$make = "$time\t\t";

		$time_formatted = $time;
		$time_formatted =~ s/:00$//g;
		$time_formatted =~ s/^0//g;

		foreach $day (@day_list){
			$hash_key = "$semester_info" . "_" . "$day" . "_" . "$time_formatted";
			$data = $hash_course{$hash_key};

			if (defined $data){
				$make .= "$data\t";
			}
			else{
				$make .= "\t";
			}
		}

		chomp($make);
		print "$make\n";
	}
}