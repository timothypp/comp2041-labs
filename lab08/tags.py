#!/usr/bin/python2.7

import urllib, re, sys

all_tags = {}
in_comment = "no"
url_to_search = ""
is_frequency = "no"

if (len(sys.argv) == 1):
	print >>sys.stderr, "Usage: %s [-f] <URL>" % sys.argv[0]
	sys.exit(1)
elif (len(sys.argv) >= 2):
	if sys.argv[1] == "-f":
		url_to_search = sys.argv[2]
		is_frequency = "yes"
	else:
		url_to_search = sys.argv[1]
		is_frequency = "no"


try:
	filehandle = urllib.urlopen(url_to_search)

	for lines in filehandle.readlines():
		lines = lines.lower()
		lines = lines.rstrip('\n')
		lines = re.sub(r'<!.*?>', "", lines)

		for tag in re.findall("<[\s]*(\w+)", lines):
			if tag in all_tags:
				total = int(all_tags[tag]) + 1
				all_tags[tag] = total
			else:
				all_tags[tag] = 1

	filehandle.close()

	if is_frequency == "no":
		for tag in sorted(all_tags):
			print tag, "", all_tags[tag]
	else:
		for tag in sorted(all_tags, key=all_tags.get, reverse=False):
			print tag, "", all_tags[tag]


except:
	print >>sys.stderr, "Invalid URL is being used!"
	sys.exit(1)
