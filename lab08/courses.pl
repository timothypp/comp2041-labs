#!/usr/bin/perl -w

$course_to_search = "";

if (@ARGV == 1){
	if ($ARGV[0] =~ '^[A-Za-z]{3,}$'){
		$course_to_search = $ARGV[0];

		getInformation($course_to_search);
	}
	else{
		die "Usage: $0 <prefix>\n";	
	}
}
else{
	die "Usage: $0 <prefix>\n";
}



sub getInformation {
	my $the_course = $_[0];
	$the_course = uc($the_course);
	my @courses_found = ();
	my %courses_final = ();

	$url = "http://www.timetable.unsw.edu.au/current/" . $the_course . "KENS.html";
	#open F, "wget -q -O- $url|" or die;
	open $fh, "-|", "wget -q -O- $url" or die;	#alternative to above

	#while ($line = <F>) {
	while ($line = <$fh>) {						#alternative to above
		push @courses_found, $line =~ /$the_course[\d]{3,}/gi;	

		foreach $course (@courses_found){
			$courses_final{$course} = 0;
		}
	}

	foreach $course (sort keys %courses_final) {
		print "$course\n";
	}

	close $fh;
}
