#!/usr/bin/perl -w

# Work by : Timothy Putra Pringgondhani (z5043683)

use strict;

my $a;
my $b;

sub reduce (&@) { 
	my ($callback) = shift;
	my (@array) = @_;

	if (@array == 0){
		return undef;
	}
	elsif (@array == 1){
		return $array[0];
	}
	else{
		my $result;

		for my $test (@array){
			if (not defined $a){
				$a = $test;
				next;
			}
			elsif (not defined $b){
				$b = $test;
			}
			
			if (defined $a and defined $b){
				$result = $callback->($a, $b);

				undef $a;
				undef $b;

				$a = $result;
			}
		}

        undef $a;
        undef $b;   

		return $result;
	}
}

my $sum = reduce { $a + $b } 1 .. 10;
my $min = reduce { $a < $b ? $a : $b } 5..10;
my $maxstr = reduce { $a gt $b ? $a : $b } 'aa'..'ee';
my $concat = reduce { $a . $b } 'J'..'P';
my $sep = '-';
my $join = reduce { "$a$sep$b" }  'A'..'E';
print "$sum $min $maxstr $concat $join\n";
