#!/usr/bin/perl -w

# Work by : Timothy Putra Pringgondhani (z5043683)

use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);

use strict;

my $max_number_to_guess = 99;

print <<eof;
Content-Type: text/html; charset=utf-8

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>A Guessing Game Player</title>
    </head>
    <body>
eof

warningsToBrowser(1);

my $low_limit   = param('low_limit');
my $high_limit  = param('high_limit');
my $guess       = param('guess');

my $high_cmd    = param('higher');
my $low_cmd     = param('lower');
my $correct_cmd = param('correct');

if (defined $high_cmd || defined $low_cmd){
    if (defined $low_limit and defined $high_limit and defined $guess) {
        $low_limit =~ s/\D//g;
        $high_limit =~ s/\D//g;
        $guess =~ s/\D//g;

        if (defined $high_cmd){
            $low_limit = $guess + 1;
        }
        elsif (defined $low_cmd){
            $high_limit = $guess - 1;
        }

        $guess = int (($high_limit + $low_limit) / 2);
    }
    else{
        $low_limit = "1";
        $high_limit = "100";
        $guess = "50";
    }

    print_play_game($low_limit, $high_limit, $guess);
}
elsif (defined $correct_cmd){
    print_win_game();
}
else{
    $low_limit = "1";
    $high_limit = "100";
    $guess = "50";

    print_play_game($low_limit, $high_limit, $guess);
}

print <<eof;
    </body>
</html>
eof

sub print_play_game{
    my $low_limit   = $_[0];
    my $high_limit  = $_[1];
    my $guess       = $_[2];

    print <<eof;
        <form method="POST" action="">
            My guess is: $guess
            <input type="submit" name="higher" value="Higher?">
            <input type="submit" name="correct" value="Correct?">
            <input type="submit" name="lower" value="Lower?">
            <input type="hidden" name="low_limit" value="$low_limit">
            <input type="hidden" name="high_limit" value="$high_limit">
            <input type="hidden" name="guess" value="$guess">
        </form>
eof
}

sub print_win_game{
    print <<eof;
        <form method="POST" action="">
            I win!!!!
           <input type="submit" value="Play Again">
        </form>
eof
}