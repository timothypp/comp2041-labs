#!/usr/bin/perl -w

# Work by : Timothy Putra Pringgondhani (z5043683)

use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);

use strict;

# Outputs a form which will rerun the script
# An input field of type hidden is used to pass an integer
# to successive invocations

my $max_number_to_guess = 99;

print <<eof;
Content-Type: text/html; charset=utf-8

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Guess A Number</title>
        <link href="./guess_number.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <center>Guess A Number</center>
                    </h1>




eof

warningsToBrowser(1);

my $number_to_guess = param('number_to_guess');
my $guess = param('guess');

my $game_over = 0;
my $page_message = "";

if (defined $number_to_guess and defined $guess) {
    $guess =~ s/\D//g;
    $number_to_guess =~ s/\D//g;
    if ($guess == $number_to_guess) {
        $page_message = "You guessed right, it was $number_to_guess.";
        $game_over = 1;
    } 
    elsif ($guess < $number_to_guess) {
        $page_message = "Its higher than $guess.";
    } 
    else {
        $page_message = "Its lower than $guess.";
    }
} 
else {
    $number_to_guess = 1 + int(rand $max_number_to_guess);
    $page_message = "I've  thought of number 0..$max_number_to_guess";
}

print <<eof;
                    <center>
                            <div class="col-xs-12 col-md-12">
                                <h4>$page_message</h4>
eof

if ($game_over) {
    print <<eof;
                            <form class="navbar-form form-group" role="search" action="" method="POST">
                                <div class="form-group">
                                    <input type="submit" class="btn" value="Play Again">    
                                </div>
                            </form>
eof
} 
else {
    print <<eof;     
                            <form class="navbar-form form-group" role="search" action="" method="POST">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="guess" placeholder="Enter value...">
                                    <input type="hidden" name="number_to_guess" value="$number_to_guess">
                                    <input type="submit" class="btn" value="Submit"> 
                                </div>
                            </form>
eof
}

print <<eof;

                        </div>
                    </center>
                </div>
            </div>
        </div>
    </body>
</html>
eof
