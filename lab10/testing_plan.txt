Testing Plan (Text)

1) ============================================================
Test Name 			: Under 16 Digits
Value in Text Field : 123
Button Pushed		: Validate
Expected Response	: 123 is invalid - does not contain exactly 16 digits ("Error")
Actual Response		: (Same)

2) ============================================================
Test Name 			: Over 16 Digits
Value in Text Field : 12345678876543210
Button Pushed		: Validate
Expected Response	: 12345678876543210 is invalid - does not contain exactly 16 digits ("Error")
Actual Response		: (Same)

3) ============================================================
Test Name 			: Alphanumeric value
Value in Text Field : abc123
Button Pushed		: Validate
Expected Response	: 123 is invalid - does not contain exactly 16 digits ("Error")
Actual Response		: (Same)

4) ============================================================
Test Name 			: 16 digits with non-digits
Value in Text Field : 2389423423423467---abc
Button Pushed		: Validate
Expected Response	: 2389423423423467 is valid ("Success")
Actual Response		: (Same)

5) ============================================================
Test Name 			: Reset from start
Value in Text Field : 1234
Button Pushed		: Reset
Expected Response	: (Empty)
Actual Response		: (Same)

6) ============================================================
Test Name 			: Reset from middle (previous value : 6789)
Value in Text Field : 1234
Button Pushed		: Reset
Expected Response	: 6789
Actual Response		: (Same)

7) ============================================================
Test Name 			: Close
Value in Text Field : 1234
Button Pushed		: Close
Expected Response	: Go to "Goodbye Page"
Actual Response		: (Same)

8) ============================================================
Test Name 			: Invalid credit card
Value in Text Field : 9182387723427777
Button Pushed		: Validate
Expected Response	: 9182387723427777 is invalid ("Error")
Actual Response		: (Same)

