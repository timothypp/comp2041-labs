#!/usr/bin/perl -w
# Work by : Timothy Putra Pringgondhani (z5043683)

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

print header, start_html("Credit Card Validation"), "\n";
warningsToBrowser(1);

my $credit_card = param("credit_card");
my $close_type = param("close");

my $credit_card_result = "";
my $textfield_text = "";

if (defined $close_type){
	chomp($close_type);
	print_close() if ($close_type eq "Close");
}

if (defined $credit_card){
	chomp($credit_card);

	$credit_card_result = validate($credit_card);

	if ($credit_card_result =~ /invalid/){
		$textfield_text = "Try again:\n";
		$credit_card_result = "<b><span style=\"color: red\">$credit_card_result</span></b>";
		$credit_card =~ s/\D//g;
	}
	else{
		$textfield_text = "Another card number:\n";
		$credit_card = ""; # Empty the textfield
	}
}
else{
	$textfield_text = "Enter credit card number:\n";
	$credit_card = ""; # Empty the textfield
}

print_body($textfield_text, $credit_card_result);





sub print_body{
	my $textfield_text = $_[0];
	my $credit_card_result = $_[1];

	# Text Part
	print h2("Credit Card Validation"), "\n";
	print "This page checks whether a potential credit card number satisfies the Luhn Formula.", "\n";
	print "<p>", "\n";

	# Credit Card Part
	print start_form, "\n";

	if ("$credit_card_result"){
		print "$credit_card_result", "\n";
		print "<p>", "\n";
	}

	print "$textfield_text", textfield(-name => "credit_card", -default => "$credit_card", -override => 1), "\n";
	print submit(-name => "submit", -value => "Validate"), "\n";
	print reset(-name => "Reset", -value => "Reset"), "\n";
	print submit(-name => "close", -value => "Close"), "\n";
	print end_form, "\n";

	print end_html;
	exit(0);
}

sub print_close{
	# Text Part
	print h2("Credit Card Validation"), "\n";
	print "Thank you for using the Credit Card Validator.", "\n";

	print end_html;
	exit(0);
}


sub luhn_checksum {
	my $number = $_[0];
	my $checksum = 0;
	my $digits = reverse $number;

	my $i = 0;
	my @enumerate = map [$i++, $_], split('', $digits); # E.g. ([0, 'a'], [1, 'b'], [2, 'c'])

	foreach my $ref (@enumerate) {
	    my($index, $digit) = @$ref;

	    my $multiplier = 1 + $index % 2;
	    my $d = int ($digit) * $multiplier;

	    if ($d > 9){
	    	$d -= 9;
	    }

	    $checksum += $d;
	}

	return $checksum
}

sub validate{
	my $credit_card = $_[0];
	$credit_card =~ s/\D//g;

	if (length($credit_card) != 16){
		return $credit_card . " is invalid  - does not contain exactly 16 digits\n"
	}
	elsif (luhn_checksum($credit_card) % 10 == 0){
		return $credit_card . " is valid\n"
	}
	else{
		return $credit_card . " is invalid\n"
	}
}
